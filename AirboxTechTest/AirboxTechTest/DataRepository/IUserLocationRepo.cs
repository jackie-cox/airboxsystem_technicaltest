﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AirboxTechTest.DataRepository
{
    public interface IUserLocationRepo
    {
        public UserLocation FindUserLocationCurrent(int userid);

        public IEnumerable<UserLocation> FindUserLocationHistory(int userid);

        public IEnumerable<UserLocation> FindAllUsersCurrentLocation();
        
        public void AddLocation(UserLocation userLocn);
        
    }
}
