﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AirboxTechTest.DataRepository
{
    public class UserLocationRepo: IUserLocationRepo
    {
        private List<UserLocation> dummyDb = new List<UserLocation>();

        public UserLocationRepo()
        {
            dummyDb.Add(
                    new UserLocation()
                    {
                        UserId = 1,
                        DateTime = DateTimeOffset.UtcNow,
                        Latitude = 51.60517716780864,
                        Longitude = -1.4448709292502147
                    });
            dummyDb.Add(
                new UserLocation()
                {
                    UserId = 1,
                    DateTime = DateTimeOffset.UtcNow.AddHours(-1),
                    Latitude = 51.60517716780864,
                    Longitude = -1.4448709292502147
                });


            dummyDb.Add(
                    new UserLocation()
                    {
                        UserId = 2,
                        DateTime = DateTimeOffset.UtcNow.AddMinutes(-5),
                        Latitude = 51.55622479852391,
                        Longitude = -0.27919927696319025
                    });
        }  

        public UserLocation FindUserLocationCurrent(int userid)
        {
            var userCurrentLocation = dummyDb.Where(u => u.UserId == userid).OrderByDescending(u => u.DateTime).FirstOrDefault();
            return userCurrentLocation;
        }

        public IEnumerable<UserLocation> FindAllUsersCurrentLocation()
        {
            var currentLocations = dummyDb.GroupBy(u=> u.UserId)
                                               .Select(s => s.OrderByDescending(u => u.DateTime).FirstOrDefault());
                                              
            return currentLocations;
        }

        public IEnumerable<UserLocation> FindUserLocationHistory(int userid)
        {
            var userCurrentLocations = dummyDb.Where(u => u.UserId == userid).OrderByDescending(u => u.DateTime).ToList();
            return userCurrentLocations;
        }

    

        public void AddLocation(UserLocation userLocn)
        {
            dummyDb.Add(userLocn);
        }
    }
}
