using System;

namespace AirboxTechTest
{
    public class UserLocation
    {
        public int UserId { get; set; }

        public DateTimeOffset DateTime { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }
    }
}
