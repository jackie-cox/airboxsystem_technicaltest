﻿using AirboxTechTest.DataRepository;
using AirboxTechTest.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace AirboxTechTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationHistoryController : Controller
    {
        private readonly ILogger<LocationCurrentController> _logger;
        private readonly IUserLocationRepo _userLocationRepo;
        
        public LocationHistoryController(ILogger<LocationCurrentController> logger, IUserLocationRepo userLocationRepo)
        {
            _logger = logger;
            _userLocationRepo = userLocationRepo;
        }

        [HttpGet("{userId}")]
        // Get location history for a specific userId
        public IEnumerable<UserLocation> Get(int userId)
        {
            try
            {
                var history = _userLocationRepo.FindUserLocationHistory(userId);
                return history;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in LocationHistory Get: {ex.Message} -> {ex.StackTrace}");

                throw new Exception(MessageHelper.GENERICEXCEPTIONMESSAGE);
            }
        }
    }
}
