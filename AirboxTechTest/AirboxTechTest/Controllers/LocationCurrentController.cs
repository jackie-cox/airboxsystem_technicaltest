﻿using AirboxTechTest.DataRepository;
using AirboxTechTest.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirboxTechTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LocationCurrentController : ControllerBase
    {

        private readonly ILogger<LocationCurrentController> _logger;
        private readonly IUserLocationRepo _userLocationRepo;

        public LocationCurrentController(ILogger<LocationCurrentController> logger, IUserLocationRepo userLocationRepo)
        {
            _logger = logger;
            _userLocationRepo = userLocationRepo;
        }

        [HttpGet]
        // Get current locations for all userId's
        public IEnumerable<UserLocation> Get()
        {
            try
            {
                var userLocn = _userLocationRepo.FindAllUsersCurrentLocation();
                return userLocn;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in Location Get: {ex.Message} -> {ex.StackTrace}");
                throw new Exception(MessageHelper.GENERICEXCEPTIONMESSAGE);
            }
        }

        [HttpGet("{userId}")]
        // Get current location for a specific userId
        public UserLocation Get(int userId)
        {
            try
            {
                if (userId == 0)
                {
                    throw new ArgumentException("userId can not be 0");
                }

                var userLocation = _userLocationRepo.FindUserLocationCurrent(userId);
                return userLocation;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in Location Get/{{userId}}: {ex.Message} -> {ex.StackTrace}");
                throw new Exception(MessageHelper.GENERICEXCEPTIONMESSAGE);
            }
        }

        [HttpGet("{radiusMeters}/{radiusCentreLatitude}/{radiusCentreLongitude}")]
        // Get current locations for all userId's that are within {radiusMeters} of the location {radiusCentreLatitude} & {radiusCentreLongitude}.
        public IEnumerable<UserLocation> Get(int radiusMeters, double radiusCentreLatitude, double radiusCentreLongitude)
        {
            // to test use https://localhost:44319/LocationCurrent/500/51.557438834180225/-0.283050928962625450
            // will return userId 2's location

            try
            {
                var userCurrentLocations = _userLocationRepo.FindAllUsersCurrentLocation().ToList();

                var usersWithinRange = new List<UserLocation>();

                foreach (UserLocation userLocation in userCurrentLocations)
                {
                    var distance = LocationHelper.CalculateDistanceMeters(radiusCentreLatitude, radiusCentreLongitude, userLocation.Latitude, userLocation.Longitude);

                    if (distance <= radiusMeters)
                    {
                        usersWithinRange.Add(userLocation);
                    }
                }
                return usersWithinRange;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in Location Get/{{radiusMeters}}/{{radiusCentreLatitude}}/{{radiusCentreLongitude}}: {ex.Message} -> {ex.StackTrace}");
                throw new Exception(MessageHelper.GENERICEXCEPTIONMESSAGE);
            }
        }

        [HttpPost]
        public UserLocation Post(int userId, string dateTimeOfLocationUtc, double longitude, double latitude)
        {
                DateTimeOffset dateTimeUtc;
                if (userId == 0)
                {
                    throw new ArgumentException("userId can not be 0");
                }

                try
                {
                    if (string.IsNullOrEmpty(dateTimeOfLocationUtc))
                    {
                        throw new Exception("dateTimeOfLocationUtc not provided");
                    }

                    dateTimeUtc = DateTime.ParseExact(dateTimeOfLocationUtc, "yyyyMMdd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    if (dateTimeUtc == DateTimeOffset.MinValue)
                    {
                        throw new Exception($"invalid dateTimeOfLocationUtc {dateTimeOfLocationUtc} converting to MinDateTime");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Error with argument dateTimeOfLocation: {ex.Message} -> {ex.StackTrace}");
                    throw new ArgumentException("Invalid dateTimeOfLocation. Please ensure it is in the format: yyyyMMdd HH:mm:ss");
                }

            // Add any error handling for latitude & longitude if requried

            try
            {
                var userLocn = new UserLocation()
                {
                    UserId = userId,
                    // DateTime = DateTimeOffset.UtcNow,
                    //Latitude = 51.50855992058224,
                    //Longitude = -0.08704545454354777,

                    DateTime = dateTimeUtc,
                    Latitude = latitude,
                    Longitude = longitude,
                };

                _userLocationRepo.AddLocation(userLocn);
                return userLocn;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in Location Post: {ex.Message} -> {ex.StackTrace}");
                throw new Exception(MessageHelper.GENERICEXCEPTIONMESSAGE);
            }
        }


    }
}
